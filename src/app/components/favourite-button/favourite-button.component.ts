import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { FavouriteService } from 'src/app/services/favourite.service'; 

@Component({
  selector: 'app-favourite-button',
  templateUrl: 'favourite-button.component.html',  
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {


  @Input() pokemonId: string = "";

  get loading() : boolean {
    return this.favouriteService.loading
  }

  constructor(
    private readonly favouriteService : FavouriteService
  ) { }

  ngOnInit(): void {
  }

  onFavouriteClick(): void {
    //Add pokemons to the favourties
    alert("Clicked a favourite: " + this.pokemonId);
    this.favouriteService.addToFavourites(this.pokemonId)
      .subscribe({
        next: (response: Trainer) => {
          console.log("NEXT", response);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
  }

  

} 