import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { DeleteService } from 'src/app/services/delete.service';

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.css']
})
export class DeleteButtonComponent implements OnInit {
  
  @Input() pokemonId: string = "";

  get loading() : boolean {
    return this.deleteService.loading
  }

  constructor(private readonly deleteService: DeleteService) { }

  ngOnInit(): void {
  }

  onDeleteFavouriteClick(): void {
    //Delete pokemons from the favourties
    alert("Clicked the delete favourite: " + this.pokemonId);
    this.deleteService.deleteFromFavourites(this.pokemonId)
      .subscribe({
        next: (response: Trainer) => {
          console.log("NEXT", response);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
    }

}
