import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';
const { apiKey, apiUsers } = environment
@Injectable({
  providedIn: 'root'
})
export class DeleteService {
  private _loading: boolean = false;
  get loading(): boolean {
    return this._loading;
  }
  constructor(
    private http: HttpClient,
    //private message: MessageService,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
  ) { }
  public deleteFromFavourites(pokemonId: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error("addToFavourites: There is no trainer")
    }
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId)
    if (pokemon) {
      throw new Error("addToFavourites: No pokemon with id: " + pokemonId)
    }

    const headers = new HttpHeaders({
      'consten-type': 'application/json',
      'x-api-key': apiKey
    })
    this._loading = true;
    return this.http.delete<Trainer>(`${apiUsers}/${/*trainer.id*/ 3}/${["ekans"/*...trainer.pokemon*/]}`, {headers});
    ///${pokemon: [...trainer.pokemon, pokemonId]}
  }
}