import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';
const { apiKey, apiUsers } = environment
@Injectable({
  providedIn: 'root'
})
export class FavouriteService {
  private _loading: boolean = false;
  get loading(): boolean {
    return this._loading;
  }
  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
  ) { }
  public addToFavourites(pokemonId: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error("addToFavourites: There is no trainer")
    }
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId)
    if (pokemon) {
      throw new Error("addToFavourites: No pokemon with id: " + pokemonId)
    }

    const headers = new HttpHeaders({
      'consten-type': 'application/json',
      'x-api-key': apiKey
    })
    this._loading = true;
    return this.http.patch<Trainer>(`${apiUsers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon, pokemonId]
    }, {
      headers
    })
    .pipe(
      tap((updateTrainer: Trainer) => {
        this.trainerService.trainer = updateTrainer
      }),
      finalize(() => {
        this._loading = false
      })
    )
  }
}