export interface Trainer {
    id: string;
    username: string;
    pokemon: string[];
    favourites: string[];   
}


