import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LandingPagePage } from './pages/landing-page/landing-page.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { LandingpageloginFormComponent } from './components/landingpagelogin-form/landingpagelogin-form.component';
import { FormsModule } from '@angular/forms';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { FavouriteButtonComponent } from './components/favourite-button/favourite-button.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DeleteButtonComponent } from './components/delete-button/delete-button.component';



@NgModule({ 
  declarations: [ //Components
    AppComponent,
    LandingPagePage,
    TrainerPage,
    PokemonCataloguePage,
    LandingpageloginFormComponent,
    PokemonListComponent,
    FavouriteButtonComponent,
    NavbarComponent,
    DeleteButtonComponent,    
  ],
  imports: [ //Modules
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
