/*import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { Dataservice } from 'src/data.service';
@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  pokemons: any[] = [];
  trainer!: Trainer;


  constructor(
    private dataservice: Dataservice,
    private trainerId: TrainerService
  ) { }
  
  ngOnInit(): void {
    this.getPokemons()
  }


  getPokemons() {
    this.dataservice.getFavouritePokemons("4")
      .subscribe((response: any) => {
        console.log(JSON.stringify(response.pokemon));
        response.pokemon.forEach((result : string) => {
          this.dataservice.getMoreData(result)
            .subscribe((uniqResponse: any) => {
              this.pokemons.push(uniqResponse);
            })
        })
      })
  }

  deletePokemon() {
    alert("delete button clicked...")
    throw new Error('Method not implemented.');
    }
}*/

import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { Dataservice } from 'src/data.service';
@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  @Input() pokemonId: string = "";
  pokemons: any[] = [];
  constructor(
    private dataservice: Dataservice,
    private readonly trainerService: TrainerService
  ) { }
  ngOnInit(): void {
    this.getPokemons()
  }
  getPokemons() {
    const trainer: Trainer | undefined = this.trainerService.trainer;
    if (trainer !== undefined) {
      this.dataservice.getFavouritePokemons(`${trainer.id}`)
        .subscribe((response: any) => {
          response.pokemon.forEach((result: string) => {
            this.dataservice.getMoreData(result)
              .subscribe((uniqResponse: any) => {
                this.pokemons.push(uniqResponse);
              })
          })
        })
    }
  }
}